# robot-model project

This project provides a library to compute the forward kinematics and link Jacobians of any robot using its URDF model.

## Objective

Using the [urdfdomcpp](https://github.com/BenjaminNavarro/urdfdomcpp) (URDF parser) and [Eigen](eigen.tuxfamily.org/) (linear algebra) libraries, implement the algorithms required to extract the pose and Jacobian of any robot link (body) based on the current joints' position.

A code skeleton is provided. Fill all functions inside *src/robot_model/robot_model.cpp* that are tagged with a `//TODO implement` comment. While implementing a function, remove any `[[maybe_unused]]` attribute as they are present only to avoid warnings.

You must **not** modify the existing `RobotModel` class public interface (functions, variables, types), but you are free to add any member function or variable. This makes sure that all unit tests in *tests/forward_kinematics/forward_kinematics.cpp* can be compiled without any modification. If you add new public member functions, please try to add unit tests for them.

For simplification, consider that all joints types are either `fixed`, `revolute` or `continuous`.

## Bonus objective

If you managed to complete everything that is listed above in time, you can try to handle other joint types, starting with `prismatic` joints. The other two, `planar` and `floating`, require modifications to the public interface since they have more than one degree of freedom. 

Any additional joint type that is properly implemented will grant you bonus points.

## How to build and test the code
From the `build` folder:
1. Configure the project with tests enabled: `cmake -DENABLE_TESTING=ON ..` (you can remove the `-DENABLE_TESTING=ON` part if you don't care about testing)extract Ja
2. Build the code: `cmake --build . --parallel`
3. To run the tests: `ctest`
4. To create a local reusable Conan package: `conan create .. user/channel` (e.g `conan create .. johndoe/stable`)

## Tips and tricks
* When you add new source files, don't forget to list them in the corresponding `CMakeLists.txt` file otherwise they won't be built
* If your IDE doesn't automatically format your code, you can run the `format.sh` script at the root of the project to do so
* You can generate the Doxygen documentation using the `doc` target: `cmake --build . --target doc`. Then just open the `build/docs/index.html` file in your browser to access it.
* You can generate the a test coverage report using the `coverage` target: `cmake --build . --target doc`. Then just open the `build/coverage/index.html` file in your browser to access it.
* To enforce good coding practice, the compiler is configured to generate more warnings and to treat all warnings as errors. Both of these aspects can be disabled using the `MORE_WARNINGS` and `WARNINGS_AS_ERRORS` CMake options.
* If a test fails, you can find its console output in `build/Testing/Temporary/LastTest.log`. Alternatively, you can run the test manually, e.g `./bin/my_test`. You can pass the `-s` option after the executable name to list successful tests
