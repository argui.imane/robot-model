#include <fmt/core.h>
#include <umrob/robot_model.h>
#include <urdf_model/pose.h>
#include <urdf_parser/urdf_parser.h>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <stdexcept>

namespace umrob {
RobotModel::RobotModel([const std::string& urdf) {
    //A l'aide du fichier URDF, on construit le modele du robot
    //On fait appel au "parent joint" et "child joint"
     model_ = urdf::parseURDF(urdf);
     auto root = model_->getRoot();
     auto joints = root->child_joints;
     auto axis = joints[0]->axis;
}

double& RobotModel::jointPosition(const std::string& name) {
    return joints_position_.at(name);
}

size_t RobotModel::jointIndex(const std::string& name) const {
    //On obtient l'index de chaque joint
    ContainerT::const_iterator iteration = joint_name_index_map_.find(name);
    return 0;
}

const std::string& RobotModel::jointNameByIndex(size_t index) const {
    return joints_position_.begin()->first; 
}

void RobotModel::update() {
    //utilise la position actuelle du jointstate pour
    //mettre à jour robotstate
}

const Eigen::Affine3d&
RobotModel::linkPose(const std::string& link_name) const {
    return links_pose_.at(link_name);
}

const Jacobian& RobotModel::linkJacobian(const std::string& link_name) {
    // Obtient la matrice jacobienne das le repère d'origine
    //pour le link donné
    return links_jacobian_.at(link_name);
}

const JointLimits&
RobotModel::jointLimits(const std::string& joint_name) const {
    return joints_limits_.at(joint_name);
}

size_t RobotModel::degreesOfFreedom() const {
    //Calcul des degrés de liberté
    //peut varier selon le nombre de joints
    return size_t;
}

std::vector<urdf::JointConstSharedPtr> RobotModel::jacobianJointChain([
    [maybe_unused]] const std::string& link_name) const {
    std::vector<urdf::JointConstSharedPtr> joint_chain;

    // 
    return joint_chain;
}

} // namespace umrob